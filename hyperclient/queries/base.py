# -*- coding: utf-8 -*-

"""
hyperclient.queries.base
~~~~~~~~~~~~~

This module contains some helpful stuff for constructing queries for resource collections.

Queries are formed from key-value pares used for searching and filtering collections of objects.
For example we have a query: action__name__contains='something'.
This means that for some object in a collection of objects attribute by path (action, name) should contain 'something'.
"""

from functools import partial

from . import processors
from .exceptions import (
    QueryException, QueryAttrDoesNotExist, QueryObjectDoesNotExist, QueryMultipleObjectsExist)


# Separator used to split query string apart
SEPARATOR = '__'
# Name of the function that an object must have to execute a query himself.
TRY_ON_QUERY_FUNCTION = 'try_on_query'


def parse_query(kwargs):
    """
    Parses a key-value pare for a query.

    :param kwargs: A single key-value pare in a dict
    :return: A tuple (path, (processor_name, processor), value).
        For example for query `action__method__startswith='What'` it will be ((action, method), 'startswith', 'What')
    """
    key, value = kwargs.items()[0]
    path_and_processor = key.split(SEPARATOR)
    if len(path_and_processor) < 2:
        raise ValueError('Query key should be in form <path_part1>__<path_part2>__...__<processor>, '
                         'cannot parse [{0}]'.format(key))

    path = path_and_processor[:-1]
    processor_name = path_and_processor[-1]

    processor = processors.get(processor_name)
    if not processor:
        raise QueryException('Cannot find a processor with name [{0}]'.format(processor_name))

    return path, (processor_name, processor), value


def attr_value_by_name(obj, name):
    """Gets obj['name'] or obj.name or raises an exception if both fails"""
    try:
        return obj[name]
    except (KeyError, TypeError):
        try:
            return getattr(obj, name)
        except AttributeError:
            raise QueryAttrDoesNotExist('No attribute with name [{0}] found for object'.format(name))


def join_to_kwargs(path, processor_name, value):
    """Joins parts of query to a dict key-value pare of a query"""
    return {SEPARATOR.join(path) + SEPARATOR + processor_name: value}


def q_to_kwargs(query):
    """
    Takes a Q object and constructs a dict key-value pare for a query like `action_method__startswith: 'What'`

    :param query: A Q object.
    :return: A one key-value pare in a dict.
    """
    return join_to_kwargs(query.path, query.processor_name, query.value)


def kwargs_to_q(kwargs):
    """
    Takes a dict of queries in the form `action_method__startswith: 'What'`
        and returns a list of corresponding Q objects.

    :param kwargs: Dict with queries.
    :return: List with corresponding Q objects.
    """
    return [Q(**{k: v}) for k, v in kwargs.iteritems()]


def args_to_q(args, kwargs):
    """
    Takes a list of Q objects and a dict of queries in the form `action_method__startswith: 'What'`
    and returns a combined list of Q objects.

    :param args: A list of Q objects.
    :param kwargs: Dict with queries.
    :return: Combined list with Q objects.
    """
    result = kwargs_to_q(kwargs)
    result.extend(args)
    return result


def try_query_on(obj, query):
    """
    Tries a query on a particular object.

    :param obj: An object from the collection that we want to test condition on.
    :param query: A Q object representing a query.
    :return: True if obj matches the query and False otherwise.
    """

    #
    # """
    # This function, when called produces an attribute (defined by path) value or raises an exception.
    # It will be passed to a query processor that will use it to get an attribute value.
    # """

    get_attr_func = None
    current_obj = obj
    for i in xrange(len(query.path)):
        # If next object along the path is a QuerySet, we
        if isinstance(current_obj, QuerySet):
            return any(current_obj.filter(Q.from_parts(query.path[i:], query.processor_name, query.value)))
        try:
            current_obj = attr_value_by_name(current_obj, query.path[i])
        except QueryAttrDoesNotExist as exc:
            def attr_not_found_func():
                """A function to pass to processor"""
                raise exc
            get_attr_func = attr_not_found_func
            break
    else:
        def attr_found_func():
            return current_obj
        get_attr_func = attr_found_func

    # Now we pass the resulted function that gets a tested value to a processor
    result = query.processor(get_attr_func, query.value)

    return not result if query.negated else result


def try_queries_on(obj, queries):
    """
    Tries specified queries on a specified object.

    :param obj: An object to try queries on.
    :param queries: A list of queries `Q` objects.
    :return: True if obj passes all queries, False otherwise.
    """
    return all([query.try_on(obj) for query in queries])


def filter_by_queries(data, queries):
    """
    Filter specified list of objects with a list of queries (ANDed together).

    :param data: A list of objects to try queries on.
    :param queries: A list of queries.
    :return: A list of filtered data.
    """
    filtered_data = []
    for obj in data:
        if try_queries_on(obj, queries):
            filtered_data.append(obj)

    return filtered_data


def filter_by_queries_gen(data, queries):
    """
    Does the same thing filter_by_queries does, but returns a generator that produces filtered objects

    :param data: A list of objects to try queries on
    :param queries: A list of queries
    :return: A generator object that produces filtered results
    """
    for obj in data:
        if try_queries_on(obj, queries):
            yield obj


class Q(object):
    """
    Is used to encapsulate a collection of keyword arguments for a SirenCollectionQuery
    >>> Q(question__startswith='What')
    """

    def __init__(self, **kwargs):
        """
        Initializes an instance

        :param kwargs: kwargs in form action_method__startswith='What'
        """
        if 0 > len(kwargs) > 1:
            raise ValueError('A single kwargs pare should be specified for Q construction,'
                             ' instead [{0}] are given'.format(len(kwargs)))

        # First part of key defines a path
        # In key action__method__startswith path is '[action, method]'
        self.path, (self.processor_name, self.processor), self.value = parse_query(kwargs)

        # Indicates if the query condition is negated
        self.negated = False

    @classmethod
    def from_parts(cls, path, processor_name, value):
        """Additional constructor that creates new Q object from parsed components."""
        return Q(**join_to_kwargs(path, processor_name, value))

    def negate(self):
        """Negate the query condition"""
        self.negated = not self.negated

    def __invert__(self):
        self.negate()
        return self

    def try_on(self, obj):
        """
        Tries a query condition on a particular object.

        :param obj: An object from the collection that we want to test condition on.
        :return: True if obj matches the condition and False otherwise.
        """
        return try_query_on(obj, self)

    def __str__(self):
        return "{0.path!r}, {0.processor_name!r}, {0.value!r}".format(self)

    def __repr__(self):
        return '<Q({0})>'.format(self)


class QuerySet(object):
    """This class represents a collection of objects managed by queries."""

    def __init__(self, data, getitem_key=None):
        """
        Initializes an instance.

        :param data: A list of siren resources objects of classes SirenLink, SirenAction, etc.
        :param getitem_key: A key that will be used in __getitem__ function as a query.
        """
        self.data = data
        self.queries = []
        self.getitem_key = getitem_key

    def filter(self, *args, **kwargs):
        """
        This function adds queries specified in `**kwargs` to this query object.

        :param args: A number of Q objects.
        :param kwargs: Query key-pares like `name__startswith='Dave'`
        :return: self with new queries added to self.queries.
        """
        self.queries.extend(args_to_q(args, kwargs))
        return self

    def exclude(self, *args, **kwargs):
        """
        This function adds negated queries specified in `**kwargs` to this query object

        :param args: A number of Q objects.
        :param kwargs: Query key-pares like `name__startswith='Dave'`
        :return: self with new queries added to self.queries.
        """
        self.queries.extend([~x for x in args_to_q(args, kwargs)])
        return self

    def get(self, *args, **kwargs):
        """
        Returns a single object that matches a specified query in **kwargs.
        If multiple objects found, MultipleObjectsExist is raised.
        If no objects found, ObjectDoesNotExist is raised.

        :param args: A number of Q objects.
        :param kwargs: A dict of queries in the form action_name__contains: 'what'.
        :return: A single object
        """
        queries = args_to_q(args, kwargs)
        queries.extend(args)
        data = self.execute_queries(queries)
        if len(data) < 1:
            raise QueryObjectDoesNotExist('No objects with specified properties found: {}'.format(queries))
        if len(data) > 1:
            raise QueryMultipleObjectsExist('More then one object [{}] is '
                                            'found for specified query.'.format(len(data)))

        return data[0]

    def first(self, *args, **kwargs):
        """
        Returns a first object that matches a specified query in **kwargs
        If no queries are specified then those in self.queries are used

        :param args: A number of Q objects.
        :param kwargs: A dict of queries in the form action_name__contains: 'what'.
        :return: First object that matches all queries or None if none found
        """
        queries = args_to_q(args, kwargs) if kwargs else self.queries
        return next(self.execute_queries_gen(queries), None)

    def last(self, *args, **kwargs):
        """
        Returns the last object that matches a specified query in **kwargs
        If no queries are specified then those in self.queries are used

        :param args: A number of Q objects.
        :param kwargs: A dict of queries in the form action_name__contains: 'what'.
        :return: Last object that matches all queries or None if none found
        """
        queries = args_to_q(args, kwargs) if kwargs else self.queries
        filtered = filter_by_queries(self.data, queries)

        return filtered[-1] if filtered else None

    def done(self):
        """
        This method executes all the queries in self.queries (see SirenCollectionQuery)
            and creates new SirenCollection based on the data returned.
        """
        if not self.queries:
            return self
        filtered_data = self.execute_queries()
        # Now we should empty the queries that have been executed
        del self.queries[:]

        return QuerySet(filtered_data)

    @property
    def collection(self):
        """A SirenCollection object that has all the queries executed"""
        return self.done()

    def __getitem__(self, item):
        # Before getting any items we make sure that all the queries in self.queries are executed
        # we use property self.collection for that

        # If we request by index or a slice we proxy it to collection.data
        if isinstance(item, (int, slice)):
            return self.collection.data[item]

        # If we request by something else we use getitem_query_key to construct a query and get an object.
        if not self.getitem_key:
            raise ValueError('Cannot get non-int or slice item from the QuerySet [{}] '
                             'because getitem_key is not set.'.format(item))

        try:
            return self.collection.get(**{self.getitem_key: item})
        except QueryException as exc:
            raise KeyError(exc)

    def __iter__(self):
        return iter(self.collection.data)

    def __len__(self):
        return len(self.collection.data)

    def __contains__(self, item):
        return item in self.collection.data

    def __nonzero__(self):
        return bool(self.collection.data)

    def count(self):
        return len(self.collection)

    @property
    def is_empty(self):
        return len(self.collection) == 0

    def execute_queries(self, queries=None):
        """
        Executes all the specified queries or all the queries in self.queries.
        Returns a new list of objects filtered with the queries.

        :param queries: If None, self.queries will be used to filter the data, if non None it will be used.
        :return: A list of filtered data.
        """
        queries = queries or self.queries or []
        return filter_by_queries(self.data, queries)

    def execute_queries_gen(self, queries=None):
        """
        Executes all the specified queries or all the queries in self.queries.
        Returns a generator that yields each object that passes through the filter.

        :param queries: If None, self.queries will be used to filter the data, if non None it will be used.
        :return: A generator that produces
        """
        queries = queries or self.queries or []
        return filter_by_queries_gen(self.data, queries)