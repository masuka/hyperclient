# -*- coding: utf-8 -*-

"""
hyperclient.queries.exceptions
~~~~~~~~~~~~~

This module contains exceptions related to working with queries.
"""


class QueryException(Exception):
    """Base exception class for all Query related exceptions"""
    pass


class QueryAttrDoesNotExist(QueryException):
    """
    Is used to indicate that some object in a queried
    collection doesn't have an attribute that we want.
    """
    pass


class QueryObjectDoesNotExist(QueryException):
    """Used when we can't find an object in a collection"""
    pass


class QueryMultipleObjectsExist(QueryException):
    """Used when we find many objects in a collection instead of one"""
    pass
