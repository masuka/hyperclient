# -*- coding: utf-8 -*-

"""
hyperclient.queries.processors
~~~~~~~~~~~~~

This module contains processors for queries.

For a query: action__name__contains='something' there should be a query processor with name 'contains'
that know how to compare values and return a right result.
"""

import re
from functools import partial

from .exceptions import QueryAttrDoesNotExist
from ..utils import silence

# A dict of all available processors.
processors = {}


def register(name, processor):
    """
    Registers a query processor by name.

    :param name: A name of the processor that will be used in queries like action__name__contains='something'.
    :param processor: A function that takes.
    :return: nothing
    """

    processors[name.lower()] = processor


def get(name):
    """
    This function gets a processor by it's name.

    :param name: A name of the processor, case insensitive.
    :return: A processor function or None if it's not found.
    """

    return processors.get(name.lower())


def reg(processor):
    """A decorator used to register functions as processor by their name"""
    reg_name = processor.func_name.strip('_')
    register(reg_name, processor)
    return processor


def reg_name(name):
    """A decorator used to register functions as processor with specific name"""
    def decorator(processor):
        # If name is not specified we use a processor function name stripped of '_'
        register(name, processor)
        return processor

    return decorator


# Makes a silence function that has specific exception_class
silence_not_found = partial(silence, exception_class=QueryAttrDoesNotExist)


def lower_if_strings(*objects):
    """
    Gets a list of objects and executes .lower() on all that are strings.

    :param objects: Objects to test.
    :return: A list of objects.lower() if object is a string.
    """
    def lower_if_string(obj):
        return obj.lower() if isinstance(obj, basestring) else obj

    return map(lower_if_string, objects)


@reg
def exact(tested, value):
    """
    Processor that matches exact equality between tested() and value

    Example: objects.get(name__exact='Beatles Blog')

    :param tested: A function that gets a tested value.
        A function instead of value is a parameter so that we can work with exceptions that
        arise when getting tested values (for example if we can't get a tested value, we
        can know that inside processor).
    :param value: A value that we test against.
        In this example action__name__contains='something' it is 'something'
    :return: True or False
    """
    return silence_not_found(tested) == value


@reg
def iexact(tested, value):
    """
    Same as `exact` but case insensitive.
    If both values are not strings compares them without modification.

    Example: objects.get(name__iexact='beatles blog')
    """

    tested_value, value = lower_if_strings(silence_not_found(tested), value)
    return tested_value == value


@reg
def contains(tested, value):
    """
    Containment test (in strings or iterables).
    Example:
        objects.get(headline__contains='Lennon')
        objects.filter(numbers_contains=19)
    """
    tested_value = silence_not_found(tested)
    return tested_value is not None and value in tested_value


@reg
def icontains(tested, value):
    """
    Case insensitive containment test.
    Works only on strings.
    Example: objects.get(headline__icontains='Lennon')
    """
    tested_value, value = lower_if_strings(silence_not_found(tested), value)
    return tested_value is not None and value in tested_value


@reg_name('in')
def in_(tested, value):
    """
    In a given list or a string.
    Example: objects.filter(id__in=[1, 3, 4])
    """
    tested_value = silence_not_found(tested)
    return tested_value in value


@reg
def gt(tested, value):
    """
    Greater than.
    Example: objects.filter(id__gt=4)
    """
    tested_value = silence_not_found(tested)
    return tested_value is not None and tested_value > value


@reg
def gte(tested, value):
    """Greater than or equal to."""
    tested_value = silence_not_found(tested)
    return tested_value is not None and tested_value >= value


@reg
def lt(tested, value):
    """Less than."""
    tested_value = silence_not_found(tested)
    return tested_value is not None and tested_value < value


@reg
def lte(tested, value):
    """Less than or equal to."""
    tested_value = silence_not_found(tested)
    return tested_value is not None and tested_value <= value


@reg
def startswith(tested, value):
    """
    Case-sensitive starts-with.
    Example: objects.filter(headline__startswith='Will')
    """
    tested_value = silence_not_found(tested)
    if tested_value is None:
        return False
    try:
        return tested_value.startswith(value)
    except AttributeError:
        return False


@reg
def istartswith(tested, value):
    """
    Case-insensitive starts-with.
    Example: objects.filter(headline__istartswith='will')
    """
    tested_value, value = lower_if_strings(silence_not_found(tested), value)
    if tested_value is None:
        return False
    try:
        return tested_value.startswith(value)
    except AttributeError:
        return False


@reg
def endswith(tested, value):
    """
    Case-sensitive ends-with.
    Example: objects.filter(headline__endswith='cats')
    """
    tested_value = silence_not_found(tested)
    if tested_value is None:
        return False
    try:
        return tested_value.endswith(value)
    except AttributeError:
        return False


@reg
def iendswith(tested, value):
    """
    Case-insensitive ends-with.
    Example: objects.filter(headline__iendswith='will')
    """
    tested_value, value = lower_if_strings(silence_not_found(tested), value)
    if tested_value is None:
        return False
    try:
        return tested_value.endswith(value)
    except AttributeError:
        return False


@reg_name('range')
def range_(tested, value):
    """
    Range test (inclusive).
    Example: objects.filter(num_seats__range=(2, 30))
    """
    tested_value = silence_not_found(tested)

    return tested_value is not None and value[0] <= tested_value <= value[1]


@reg
def regex(tested, value):
    """
    Case-sensitive regular expression match.
    Example: objects.get(title__regex=r'^(An?|The) +')
    """
    tested_value = silence_not_found(tested)
    return tested_value is not None and re.match(value, tested_value) is not None


@reg
def iregex(tested, value):
    """
    Case-insensitive regular expression match.
    Example: objects.get(title__regex=r'^(an?|the) +')
    """
    tested_value = silence_not_found(tested)
    return tested_value is not None and re.match(value, tested_value, re.IGNORECASE) is not None


@reg
def isnone(tested, value):
    """
    Checks if the value is None or not.
    Example: objects.get(title_isnone=False)
    """
    try:
        tested_value = tested()
    except QueryAttrDoesNotExist:
        return False
    else:
        return value == tested_value is None


@reg
def isabsent(tested, value):
    """
    Checks if tested value does not exist.
    Example: objects.get(title_isabsent=True)
    """
    try:
        tested()
    except QueryAttrDoesNotExist:
        return value
    else:
        return not value
