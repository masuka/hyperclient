# -*- coding: utf-8 -*-

"""
Hyperclient queries HTTP library
~~~~~~~~~~~~~~~~~~~~~

Queries are used to filter objects from collections.
Like this: objects.filter(name__startswith='Alex')
"""