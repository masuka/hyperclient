# -*- coding: utf-8 -*-

"""
hyperclient.hooks
~~~~~~~~~~~~~

This module contains some useful hooks tht can be used to process request and responses in HyperClient
"""

import logging
from urlparse import urlparse
from requests.status_codes import codes

from .auth import NoneAuth
from .formats import NoneFormat

log = logging.getLogger(__name__)


class BasicHook(object):
    """Basic class for hooks"""

    def __init__(self, client=None):
        self.client = client

    def __call__(self, env):
        raise NotImplemented('Hooks must be callable.')


class MethodBasedResponseHook(BasicHook):
    """
    This class specified response processing on per-method basis.
    Each response is handled by a separate HTTP method-specific handle
    """

    def __call__(self, env):
        handler = self.get_handler(env['req']['method'])
        if handler:
            handler(env)

    def get_handler(self, method):
        """Returns a handler function for a specific http method for a response"""

        handle_name = 'handle_{method}'.format(method=method.lower())
        if not hasattr(self, handle_name):
            return None

        return getattr(self, handle_name)

    def handle_post(self, env):
        pass

    def handle_get(self, env):
        pass

    def handle_update(self, env):
        pass

    def handle_delete(self, env):
        pass

    def handle_put(self, env):
        pass


class SameEndpointRequestHook(BasicHook):
    """
    If request is made to url that is outside from client.endpoint (outside api) then we need to:
        - use NoneAuth so that we don't send our auth info outside api.
        - not use client.hformat as the url is outside of api.
    """

    def __call__(self, env):
        if not self.is_same_endpoint_url(env['req']['url']):
            env['hformat'] = NoneFormat()
            env['req']['auth'] = NoneAuth()

    def is_same_endpoint_url(self, url):
        """Determines if url specified is inside api endpoint"""
        parsed_endpoint, parsed_url = urlparse(self.client.endpoint), urlparse(url)
        # If endpoint is not set (it's netloc is empty) then we assume that any requests are from this endpoint.
        # If scheme and netloc of endpoint and url are the same we assume that url is from same endpoint.
        return not parsed_endpoint.netloc or \
               (parsed_endpoint.scheme, parsed_endpoint.netloc) == (parsed_url.scheme, parsed_url.netloc)


class FormatRequestHook(BasicHook):
    """This class represents a usual way of handling requests for any hypermedia format."""

    def __call__(self, env):
        env['hformat'].process_request(req=env['req'])


class FormatResponseHook(MethodBasedResponseHook):
    """This class defines the way hypermedia format is applied to responses"""

    def handle_post(self, env):
        res = env['res']
        if res.status_code == codes.created:
            env['res'] = self.client.request('GET', res.headers['Location'])
        else:
            self.handle_get(env)

    def handle_get(self, env):
        env['res'] = env['hformat'].process_response(res=env['res'])

    def handle_update(self, env):
        self.handle_post(env)

    def handle_delete(self, env):
        res = env['res']
        if res.status_code not in (codes.okay, codes.no_content):
            log.warning('Got a response on DELETE request with status code {code}, '
                        'should be 200 or 204'.format(code=res.status_code))

    def handle_put(self, env):
        res = env['res']
        if res.status_code not in (codes.okay, codes.created, codes.no_content):
            log.warning('Got a response on PUT request with status code {code}, '
                        'should be 200, 201 or 204'.format(code=res.status_code))
