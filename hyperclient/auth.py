# -*- coding: utf-8 -*-

"""
hyperclient.auth
~~~~~~~~~~~~~

This module contains some useful auth classes to use with HyperClient
"""

import requests
import time
import logging
import json
from requests.auth import AuthBase
from requests.exceptions import HTTPError

log = logging.getLogger(__name__)


class NoneAuth(AuthBase):
    """A stub for no Auth at all, doesn't modify a"""

    def __call__(self, req):
        return req


class OAuth2Auth(AuthBase):
    """
    This class lets you authenticate using OAuth2 protocol with grant_type = password
    """

    def __init__(self, auth_url, client_keys, user_creds, verify=True):
        """
        Initializes OAuth2Base

        :param auth_url: The http endpoint for getting oauth access token
        :param client_keys: Client key and client secret in a tuple
        :param user_creds: User credentials (username, password) in s tuple
        """
        self.auth_url = auth_url
        self.client_keys = client_keys
        self.user_creds = user_creds

        # This is a thing we will use for authentication
        self.token = None

        # A session that will be used for requests to auth_url http endpoint to get token
        self.session = requests.Session()
        self.session.verify = verify

    @property
    def expired(self):
        """Indicates if the token is expired (about to expire) and needs to be refreshed"""
        return self.time_to_live < 5

    @property
    def time_to_live(self):
        """Seconds left for access token to expire"""
        return self.expires_in - self.time_lived

    @property
    def time_lived(self):
        """Time of life in seconds for current access token"""
        return time.time() - self.time_updated

    def get_token(self):
        """Gets the access and refresh tokens from http auth_url endpoint"""

        payload = {'grant_type': 'password', 'username': self.user_creds[0], 'password': self.user_creds[1]}
        log.debug('Getting token with payload {0} and for client {1}'.format(payload, self.client_keys))
        response = self.session.post(self.auth_url, data=payload, auth=self.client_keys)
        response.raise_for_status()

        self._save_new_token(response)

    def update_token(self):
        """Updates an access token, if update fails tries to get a token again"""
        if not self.token:
            return self.get_token()

        payload = {'grant_type': 'refresh_token', 'refresh_token': self.refresh_token}
        log.debug('Updating token with payload {0} and with client {1}'.format(payload, self.client_keys))
        response = self.session.post(self.auth_url, data=payload, auth=self.client_keys)
        try:
            response.raise_for_status()
        except HTTPError:
            log.warning('Exception while updating token, response body: {0}'.
                        format(json.dumps(response.json(), indent=4, sort_keys=True)
                               if 'json' in response.headers['content-type']
                               else response.text))
            self.get_token()

        self._save_new_token(response)

    def _save_new_token(self, response):
        data = response.json()
        self.token = data['access_token']
        self.refresh_token = data['refresh_token']
        self.expires_in = data['expires_in']
        self.time_updated = time.time()

    def __call__(self, req):
        if not self.token or self.expired:
            self.update_token()

        req.headers['Authorization'] = 'Bearer {0}'.format(self.token)

        return req
