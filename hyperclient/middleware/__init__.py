# -*- coding: utf-8 -*-

"""
Hyperclient middleware HTTP library
~~~~~~~~~~~~~~~~~~~~~

This package contains all the basics for construction of middleware for the hyperclient.
Including basic middleware client itself.
"""