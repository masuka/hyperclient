# -*- coding: utf-8 -*-

"""
hyperclient.middleware.client
~~~~~~~~~~~~~

This module contains some base client class.
"""

import requests
import logging
from collections import OrderedDict

from ..utils import reverse_ordered_dict

log = logging.getLogger(__name__)


class MiddlewareClient(object):
    """An HTTP client on top of requests that has a middleware stack"""

    def __init__(self):
        # An ordered list of middleware classes with names.
        self.middleware = OrderedDict()

        # This method adds .get .post .head etc. methods to the client object.
        self._add_req_methods()

    def __getattr__(self, item):
        """This will provide easy access to different middleware"""
        try:
            return self.middleware[item]
        except KeyError:
            raise AttributeError('No attribute or middleware with name [{}]'.format(item))

    def request(self, method, url, **kwargs):
        """
        Performs HTTP request with a specified method and for specified url

        :param method: An HTTP method name as 'POST', 'GET', etc.
        :param url: An url to make a request.
        :param kwargs: Any kwargs for `sessions.request` function.
        """

        # Adding url and method keys to kwargs
        kwargs['url'], kwargs['method'] = url, method

        # Create a request-response environment that acts like a context
        # Its parts can be changed during hooks processing.
        env = dict(
            client=self,
            request_kwargs=kwargs,
            session=requests.Session(),
            response=None,
            result=None,
        )

        # Now execute the request middleware and get back OrderedDict of processed middleware.
        processed = self._process_request_middleware(self.middleware, env)

        # If env['response'] is set then we do not do a request ourselves.
        env.setdefault('response', env['session'].request(**env['request_kwargs']))

        # Execute middleware for a response in reversed order.
        self._process_response_middleware(reverse_ordered_dict(processed), env)

        return env['result']

    @staticmethod
    def _process_request_middleware(middleware, env):
        """
        Execute middleware stack for outgoing request.
        If response attribute has been set during execution then execution halts
        and the response is processed through response middleware stack.
        """
        processed = OrderedDict()  # Holds processed steps.
        for name, mdw in middleware.iteritems():
            log.debug('Executing request middleware [{}] with env {}'.format(name, env))
            mdw.process_request(env)
            processed[name] = mdw
            # If response have been set we assume that request has been made inside mdw.
            if env['response']:
                break

        return processed

    @staticmethod
    def _process_response_middleware(middleware, env):
        """Executes middleware stack for a response."""
        for name, mdw in middleware.iteritems():
            log.debug('Executing response middleware [{}] with env {}'.format(name, env))
            mdw.process_response(env)
