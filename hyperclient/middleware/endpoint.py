# -*- coding: utf-8 -*-

"""
hyperclient.middleware.endpoint
~~~~~~~~~~~~~

This module contains endpoint middleware class.
"""

import types
from collections import OrderedDict


class EndpointMiddleware(object):
    """Class represents a middleware layer for MiddlewareClient."""

    def process_request(self, env):
        """
        Processes a request by modifying request context params in env dict.
        Like request_kwargs and session object.

        :param env: A dictionary with some request context, see MiddlewareClient for details.
        :return: None.
        """
        pass

    def process_response(self, env):
        """
        Processes a response by modifying response context params in env dict.
        Like response object or result.

        :param env: A dictionary with request and response context, see MiddlewareClient for details.
        :return: None.
        """
        pass
