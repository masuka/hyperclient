# -*- coding: utf-8 -*-

"""
Hyperclient HTTP library
~~~~~~~~~~~~~~~~~~~~~

Hyperclient is a client for Hypermedia APIs

"""

__title__ = 'hyperclient'
__version__ = '0.0.1'
__author__ = 'Timur Gergiya'

# Set default logging handler to avoid "No handler found" warnings.
import logging
from logging import NullHandler

logging.getLogger(__name__).addHandler(NullHandler())

from .base import client