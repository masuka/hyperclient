# -*- coding: utf-8 -*-

"""
hyperclient.utils
~~~~~~~~~~~~~

This module contains some useful utility methods and classes
"""

import json
import keyword
import re
from collections import OrderedDict


def dict_to_json(data, indent=4, sort_keys=True):
    """
    Makes a python dict to a well formatted json string

    :param data: A dict to format
    :param indent: An tab indent in spaces to use for formatting
    :param sort_keys: Weather to place keys in sorted order in json
    :param data: A dict to format
    :return: A string that holds json representation
    """

    return json.dumps(data, indent=indent, sort_keys=sort_keys)


def silence(func, exception_class):
    """
    Executes a function and returns None if exception with exception_class was thrown, silencing it.

    :param func: A function to execute.
    :param exception_class: A class of an exception to catch.
    :return: Whatever func() returns or None if exception with exception_class was thrown.
    """

    try:
        return func()
    except exception_class:
        return None


def correct_name_for_attribute(name, fix_character='_'):
    """
    When we set an attribute to an object we must make sure it's name is not in the list
    of Python restricted keywords or contain invalid characters like -,?,! - any none word characters.
    This function takes a proposed name deletes all non-word characters and if it's a keyword prepends it with a
    postfix making it a legal attribute name.

    :param name: A string name for an attribute.
    :param fix_character: A character that will be used instead of all the invalid attribute characters and as a
        postfix to add to the name in case the name is in the list of keywords names.
    :return: A string with new name.
    """
    # Replacing all non-word characters with fix_character
    name = re.sub('\W', fix_character, name)
    # If name starts from a digit, prepend with a fix_character
    if re.match('^\d', name):
        name = fix_character + name
    return name + fix_character if name in keyword.kwlist else name


def reverse_ordered_dict(odict):
    """Takes and OrderedDict and returns a new OrderedDict in reverse order."""
    items = odict.items()
    items.reverse()

    return OrderedDict(items)


class memoized_property(object):
    """A property decorator that will hash the results of property method execution."""

    def __init__(self, fget=None, fset=None, fdel=None, doc=None):
        if doc is None and fget is not None and hasattr(fget, '__doc__'):
            doc = fget.__doc__
        self.fget = fget
        self.fset = fset
        self.fdel = fdel
        self.__doc__ = doc
        if fget is not None:
            self._attr_name = '___' + fget.func_name

    def __get__(self, instance, owner=None):
        if instance is None:
            return self
        if self.fget is None:
            raise AttributeError('unreadable attribute')

        if not hasattr(instance, self._attr_name):
            result = self.fget(instance)
            setattr(instance, self._attr_name, result)
        return getattr(instance, self._attr_name)

    def __set__(self, instance, value):
        if self.fset is None:
            raise AttributeError("can't set attribute")
        delattr(instance, self._attr_name)
        return self.fset(instance, value)

    def __delete__(self, instance):
        if self.fdel is None:
            raise AttributeError("can't delete attribute")
        delattr(instance, self._attr_name)
        return self.fdel(instance)

    def getter(self, fget):
        return type(self)(fget, self.fset, self.fdel, self.__doc__)

    def setter(self, fset):
        return type(self)(self.fget, fset, self.fdel, self.__doc__)

    def deleter(self, fdel):
        return type(self)(self.fget, self.fset, fdel, self.__doc__)