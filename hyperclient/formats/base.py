# -*- coding: utf-8 -*-

"""
hyperclient.formats.base
~~~~~~~~~~~~~

This module contains base formats classes
"""

formats = {}


def register(name, hformat):
    """
    Registers a format class by name in formats package

    :param name: A name of the format, case insensitive
    :param hformat: A format class that can be a subclass of :class:`HyperFormat`
    :return: nothing
    """

    verify(hformat)
    formats[name.lower()] = hformat


def get(name):
    """
    This function gets a format by it's name
    the format has to be registered for that via ``register`` function

    :param name: A name of the registered format, case insensitive
    :return: A format object or None if it's not found
    """

    return formats.get(name.lower())


def verify(hformat, raises=False):
    """
    Verifies provided format to have necessary methods to be used as a format

    :param hformat: a format object
    :param raises: if set to True an exception will be raised in case format is invalid
    :return: True if format is valid, False if not
    """

    needed_attributes = ('process_request', 'process_response')
    valid = all(hasattr(hformat, x) for x in needed_attributes)
    if not valid and raises:
        raise InvalidFormat('Provided format is invalid', hformat)

    return valid


def identify(hformat, raises=False):
    """
    Is used to identify a format either by registered name either by format object

    :param hformat: Can be a string name of the format registered with ``formats.register``
        or a format object
    :param raises: if set to True an exception will be raised in case format is invalid or cannot be found
    :return: True if format is valid, False if not
    """

    if isinstance(hformat, basestring):
        found = get(hformat)
        if not found and raises:
            raise InvalidFormat('Format cannot be found, not registered', hformat)
        return found
    else:
        verify(hformat, raises=raises)
        return hformat


class Format(object):
    """Basic class for all hypermedia formats"""

    def __init__(self, client=None):
        self.client = client

    def process_request(self, req):
        raise NotImplemented

    def process_response(self, res):
        raise NotImplemented


class NoneFormat(Format):
    """A stub for no format at all"""

    def process_request(self, req):
        return req

    def process_response(self, res):
        return res


class BasicFormat(Format):
    """A format that follows general rules about content type"""

    content_type = None

    def process_request(self, req):
        req.setdefault('headers', {})
        req['headers'].setdefault('Accept', self.content_type)
        return req

    def process_response(self, res):
        if res.headers.get('Content-Type', '') == self.content_type:
            return self.apply_format(res)

        return res

    def apply_format(self, res):
        raise NotImplemented('Apply format should be overridden for every format')


class FormatException(Exception):
    """Basic exception class for all formats related errors"""


class InvalidFormat(FormatException):
    """Raised if format is invalid"""

