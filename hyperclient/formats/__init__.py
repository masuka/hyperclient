# -*- coding: utf-8 -*-

from .base import (
    NoneFormat, FormatException, register, get, verify, identify
)

from .siren import SirenJsonFormat


# Registering the formats
register('None', NoneFormat())
register('siren+json', SirenJsonFormat())
