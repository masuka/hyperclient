# -*- coding: utf-8 -*-

"""
hyperclient.formats.siren
~~~~~~~~~~~~~

This module contains Siren format classes

For Siren hypermedia format specification see https://github.com/kevinswiber/siren
"""

import logging
from copy import deepcopy
from collections import defaultdict

from .base import BasicFormat, FormatException
from ..utils import dict_to_json, correct_name_for_attribute
from ..queries.base import QuerySet

log = logging.getLogger(__name__)


class SirenJsonFormat(BasicFormat):
    """This a class that is used in hyperclient to represent a Siren hypermedia format."""

    content_type = 'application/vnd.siren+json'

    # This is used for custom classes support when creating SirenObjects with classes Siren attribute.
    classes = {}

    def apply_format(self, res):
        return make_object_from_resource(SirenResponseResource(self.client, res))


def make_object_from_resource(resource):
    """
    This function constructs a SirenObject by adding needed attributes and methods to it from data.

    :param resource: A SirenResource object.
    :return: A new SirenObject.
    """

    # First we get classes from resource and check if they exist in SirenFormat.classes
    # If class exists we use it in bases tuple when creating new object.
    # If class does not exist we create new class with that name.

    # First lets convert classes names from resource to str (from unicode)
    resource_classes = []
    for class_name in resource.classes:
        try:
            resource_classes.append(str(class_name))
        except UnicodeEncodeError:
            continue

    bases = []  # This list will hold classes to be bases of a new SirenObject class.
    for name in resource_classes:
        bases.append(resource.client.hformat.classes.get(name, type(name, (object,), {})))
    bases = tuple(bases)

    # Now creating a class for our SirenObject and it's instance.
    cls = type('SirenObject', bases, {})
    obj = cls()

    # Adding properties.
    for key, value in resource.properties.iteritems():
        setattr(obj, correct_name_for_attribute(key), value)

    # Adding links.
    for link in resource.links:
        for name in link.rel:
            setattr(obj, correct_name_for_attribute(name), link)

    # Adding actions.
    for action in resource.actions:
        setattr(obj, correct_name_for_attribute(action.name), action)

    # Adding entities.
    # We organise entities in such a way that obj[rel] will return a list of SirenObjects with that rel.
    entity_obj_list = [make_object_from_resource(ent) for ent in resource.entities]
    entity_obj_dict = defaultdict(list)
    for entity, entity_object in zip(resource.entities, entity_obj_list):
        for name in entity.rel:
            entity_obj_dict[name].append(entity_object)

    # Adding SirenResource as a.
    setattr(obj, '_', resource)

    # Now we should set __getitem__ magic method for SirenObject so that it can be used as a collection.
    def __getitem__(self, item):
        if isinstance(item, (int, slice)):
            return entity_obj_list[item]
        return entity_obj_dict[item]
    setattr(cls, '__getitem__', __getitem__)

    # And an iter magic method so that SirenObject can be iterable.
    def __iter__(self):
        return iter(entity_obj_list)
    setattr(cls, '__iter__', __iter__)

    # Adding __str__ and __repr__ functions
    setattr(cls, '__str__', lambda self: str(resource))
    setattr(cls, '__repr__', lambda self: '<{0.__class__.__name__}: {0}>'.format(self))

    return obj


class SirenBase(object):
    """Base class for all other Siren classes"""

    # This value will be used in a __getitem__ of a SirenCollection.
    # For example if getitem_key = 'rel__contains' then for a collection of these objects
    # collection[name] will be equivalent to collection.get(rel__contains=name)
    getitem_key = None

    def __init__(self, client, data):
        self.client = client
        self.data = data

    def __str__(self):
        return dict_to_json(self.data)

    def __repr__(self):
        return '<{0}: {1}>'.format(self.__class__.__name__, self)


class SirenResource(SirenBase):
    """A basic class for any Siren formatted resource"""

    def __init__(self, client, data=None):
        """
        Initializes an object.

        :param client: A hyperclient instance that is used to get this resource.
        :param data: A dict with data of the resource (response.json()).
        """
        super(SirenResource, self).__init__(client, data)

        # These are the parts of Siren specification for a format.

        # Describes the nature of an entity's content based on the current representation.
        # Possible values are implementation-dependent and should be documented.
        # MUST be an array of strings.
        # Optional.
        self.classes = data.get('class', [])

        # A set of key-value pairs that describe the state of an entity.
        # In JSON Siren, this is an object such as { "name": "Kevin", "age": 30 }.
        # Optional.
        self.properties = data.get('properties', {})

        # A collection of related sub-entities.
        # In JSON Siren, this is represented as an array.
        # Optional.
        self.entities = SirenCollection(client, data.get('entities', []), cls=SirenEntity)

        # A collection of action objects, represented in JSON Siren as an array such as { "actions": [{ ... }] }.
        # Optional.
        self.actions = SirenCollection(client, data.get('actions', []), cls=SirenAction)

        # A collection of items that describe navigational links, distinct from entity relationships.
        # Link items should contain a rel attribute to describe the relationship
        # and an href attribute to point to the target URI.
        # Entities should include a link rel to self.
        # In JSON Siren, this is represented as "links": [{ "rel": ["self"], "href": "http://api.x.io/orders/1234" }]
        # Optional.
        self.links = SirenCollection(client, data.get('links', []), cls=SirenLink)

        # Several aliases
        self.property = self.properties
        self.entity = self.entities
        self.action = self.actions
        self.link = self.links
        # We cannot write self.class in python, that will through SyntaxException, so we set it like this.
        setattr(self, 'class', self.classes)

        self._ = make_object_from_resource(self)

    def __str__(self):
        # If resource has too many entities we do not show them
        data = deepcopy(self.data)
        if self.entities.count() > 200:
            data['entities'] = '...too many entities to show: {0}...'.format(self.entities.count())
        return dict_to_json(data)


class SirenResponseResource(SirenResource):
    """This class represents a Siren resource in Siren format"""

    def __init__(self, client, res):
        """
        Initializes an instance

        :param client: A hyperclient instance that is used to get this resource
        :param res: An http response object that we want to use for resource creation
        """

        super(SirenResponseResource, self).__init__(client, res.json())
        self.response = res


class SirenCollection(SirenBase, QuerySet):
    """This class represents a collection of Siren resources, such as links, actions, entities"""

    def __init__(self, client, data, cls):
        """
        Initializes an instance

        :param client: A hyperclient instance that is used to get this resource.
        :param data: A list of siren resources such as links, actions, entities
            as dicts (json) or as specific classes like SirenLink, SirenAction, etc.
        :param cls: A class of objects in the collection (SirenLink for example).
        """
        data = [item if isinstance(item, cls) else cls(client, item) for item in data]
        self.cls = cls

        SirenBase.__init__(self, client, data)
        QuerySet.__init__(self, data, getitem_key=cls.getitem_key)

    def done(self):
        result = super(SirenCollection, self).done()
        return SirenCollection(self.client, result.data, self.cls)

    def __str__(self):
        return dict_to_json([x.data for x in self.collection.data])


class SirenEntity(SirenResource):
    """Represents an entity inside Siren resource"""

    getitem_key = 'rel__contains'

    def __init__(self, client, data=None):
        super(SirenEntity, self).__init__(client, data)

        # The URI of the linked sub-entity.
        # Required.
        self.href = self.data.get('href', None)
        # todo: make this validation optional?
        # if self.href is None:
        #     raise FormatException('SirenEntity has to have a href attribute: {0}'.format(self))

        # Defines the relationship of the sub-entity to its parent, per Web Linking (RFC5899).
        # MUST be an array of strings.
        # Required.
        self.rel = self.data.get('rel', [])
        if not self.rel:
            raise FormatException('SirenEntity has to have a rel attribute: {0}'.format(self))

    def follow(self):
        if self.href:
            return self.client.get(self.href)

        if self.links['self']:
            return self.links['self']()

        # Siren entity must have either href or a self link
        raise FormatException('Siren entity does not have a href and links with self relation, cannot follow it')


class SirenAction(SirenBase):
    """Represents a Siren action that resides in SirenResources and some entities"""

    getitem_key = 'name__exact'

    def __init__(self, client, data):
        super(SirenAction, self).__init__(client, data)

        # A string that identifies the action to be performed.
        # Action names MUST be unique within the set of actions for an entity.
        # The behaviour of clients when parsing a Siren document that violates this constraint is undefined.
        # Required.
        self.name = self.data.get('name', None)
        if self.name is None:
            raise FormatException('SirenAction should have a name: {0}'.format(self))

        # Describes the nature of an action based on the current representation.
        # Possible values are implementation-dependent and should be documented.
        # MUST be an array of strings.
        # Optional.
        self.classes = self.data.get('class', [])

        # An enumerated attribute mapping to a protocol method.
        # For HTTP, these values may be GET, PUT, POST, DELETE, or PATCH.
        # As new methods are introduced, this list can be extended.
        # If this attribute is omitted, GET should be assumed.
        # Optional.
        self.method = self.data.get('method', 'GET').encode('ascii')

        # The URI of the action.
        # Required.
        self.href = self.data.get('href', None)
        if self.href is None:
            raise FormatException('SirenAction should have a href: {0}'.format(self))

        # Descriptive text about the action.
        # Optional.
        self.title = self.data.get('title', None)

        # A collection of fields, expressed as an array of objects in JSON Siren such as { "fields" : [{ ... }] }.
        # Optional.
        # todo: fields are more complex then this, need to fix
        self.fields = self.data.get('fields', [])

        # The encoding type for the request.
        # When omitted and the fields attribute exists, the default value is application/x-www-form-urlencoded.
        # Optional
        self.type = self.data.get('type', None)
        if self.type is None and self.fields:
            self.type = 'application/x-www-form-urlencoded'

        # Describes the nature of an action based on the current representation.
        # Possible values are implementation-dependent and should be documented.
        # MUST be an array of strings.
        # Optional.
        self.classes = self.data.get('class', [])

        # If a field has a value it means that it is a default value for this field
        self.default_fields = {field['name']: field['value'] for field in self.fields if 'value' in field}

    def __call__(self, fields=None, defaults=True, **kwargs):
        """
        Executes this action via calling a _client and passing needed params and using right methods.

        :param fields: Dict with fields names and values of this action to send in request as data.
        :param defaults: If True, uses default values of action fields (if any) that were not set in params.
        :param kwargs: kwargs for hyperclient request method (GET, POST, etc.)
        :return: Whatever hyperclient will return after http request is executed.
        """
        fields = fields or {}
        fields = self.add_defaults(fields) if defaults else fields

        log.debug('Calling action [{0}] with fields [{1}]'.format(self.name, fields))

        # Using setdefault because all the values can be set in kwargs explicitly
        # In that case they have a priority over all else

        # Some HTTP requests should not have bodies and we need to pass all fields in uri params
        if self.method in ('GET', 'HEAD', 'OPTIONS', 'DELETE'):
            # Also we need to keep the params that were set explicitly in kwargs (if they were set)
            kwargs.setdefault('params', {})
            # Params specified in kwargs explicitly have go before those found in an action
            fields.update(kwargs['params'])
            kwargs['params'] = fields
        else:
            # If HTTP request can have body then we pass fields in it's body as a json
            kwargs.setdefault('json', {})
            # If json kwarg already has a dict in it we update it with our data also.
            # Again fields should be overwritten by data fields
            fields.update(kwargs['json'])
            kwargs['json'] = fields

            # Also, for HTTP requests with body we set a Content-Type header
            kwargs.setdefault('headers', {})
            kwargs['headers'].setdefault('Content-Type', self.type)

        res = self.client.request(self.method, self.href, **kwargs)

        return res

    def add_defaults(self, fields):
        """This methods adds default value of fields to fields dict, returns a new dict"""
        defaults = deepcopy(self.default_fields)
        defaults.update(fields)
        return defaults


class SirenLink(SirenBase):
    """Represents a Siren link that can be found in resources and entities"""

    getitem_key = 'rel__contains'

    def __init__(self, client, data):
        super(SirenLink, self).__init__(client, data)

        # Defines the relationship of the link to its entity, per Web Linking (RFC5988).
        # MUST be an array of strings.
        # Required.
        self.rel = self.data.get('rel', [])
        if not self.rel:
            raise FormatException('SirenLink should have a rel attribute: {0}'.format(self))

        # The URI of the linked resource.
        # Required.
        self.href = self.data.get('href', None)
        if not self.href:
            raise FormatException('SirenLink should have a href attribute: {0}'.format(self))

        # Describes aspects of the link based on the current representation.
        # Possible values are implementation-dependent and should be documented.
        # MUST be an array of strings.
        # Optional.
        self.classes = self.data.get('class', [])

        # Text describing the nature of a link.
        # Optional.
        self.title = self.data.get('title', None)

        # Defines media type of the linked resource, per Web Linking (RFC5988).
        # Optional.
        self.type = self.data.get('type', None)

    def __call__(self, **kwargs):
        """
        Follows this link using _client get method

        :param kwargs: Params to pass for _client hyperclient for get method
        :return: Whatever _client.get method returns
        """
        log.debug('Following link: {0} with params [{1}]'.format(self, kwargs))

        return self.client.get(self.href, **kwargs)

    def ping(self, **kwargs):
        """
        Sends a HEAD request on the

        :param kwargs: Params to pass for _client hyperclient for head method
        :return: Whatever _client.get method returns
        """
        log.debug('Pinging link: {0} with params [{1}]'.format(self, kwargs))

        # Allowing redirects for HEAD request
        kwargs.setdefault('allow_redirects', True)
        return self.client.head(self.href, **kwargs)
