# -*- coding: utf-8 -*-

"""
hyperclient.base
~~~~~~~~~~~~~

This module contains the base client class
"""

import requests
import types
import logging
from urlparse import urljoin
from collections import OrderedDict

from . import formats
from .hooks import FormatRequestHook, FormatResponseHook, SameEndpointRequestHook


log = logging.getLogger(__name__)


def client(*args, **kwargs):
    return HyperClient(*args, **kwargs)


class HyperClient(object):
    """Class representing a client for Hypermedia APIs"""

    _http_methods_ = ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD']

    def __init__(self, endpoint='', hformat='None', auth=None, verify=True):
        # The api endpoint
        self.endpoint = endpoint

        # The hypermedia format that should be used when accessing api on the endpoint.
        # Can be a string name of registered format via `formats.register` or an actual `formats.Format` class.
        self.hformat = hformat

        # A requests.session object to use for request to api.
        self.session = requests.Session()

        # The auth we will use to access resources on the endpoint.
        self.auth = auth

        # If we need to verify the SSL certificate of the server
        self.verify = verify

        # Hooks that will be fired before sending requests and after getting responses
        self.hooks = dict(request=OrderedDict(), response=OrderedDict())

        # Adding format request and format response hooks
        # These hooks are special, they we will executed after all others
        self.add_hook('response', 'hformat', FormatResponseHook())
        self.add_hook('request', 'hformat', FormatRequestHook())

        # This hook will set auth and hformat to NoneAuth and NoneFormat if we make request outside api endpoint
        self.add_hook('request', 'same_endpoint', SameEndpointRequestHook())

        # This method adds .get .post .head etc. methods to the client object.
        self._add_req_methods()

    def _dispatch_hooks(self, key, env):
        """Executes all hooks of specific type defined by key param"""

        # hformat hooks get special treatment, they always go last
        hooks = self.get_hooks(key)
        for name, hook in hooks.iteritems():
            if name == 'hformat':
                continue
            log.debug('Executing hook [{0}]'.format(name))
            hook(env)

        hf_hook = hooks.get('hformat', None)
        if hf_hook:
            log.debug('Executing hook [hformat]')
            hf_hook(env)

    def _add_req_methods(self):
        """Used to add method function for all types of HTTP methods in _methods_ attr"""
        for method in self._http_methods_:
            setattr(self, method.lower(), types.MethodType(self._get_req_method(method), self))

    @staticmethod
    def _get_req_method(method):
        """Constructs a particular method function by HTTP method name like .get .post"""
        def _req(obj, *args, **kwargs):
            return obj.request(method, *args, **kwargs)
        return _req

    def get_hooks(self, key):
        """Gets hooks dict of specified type by `key` else raises an exception"""
        hooks = self.hooks.get(key)
        if hooks is None:
            raise ValueError('Trying to operate on an unsupported type of hooks [{0}]'.format(key))

        return hooks

    def add_hook(self, key, name, hook):
        """
        Adds a hook of a specified type to be triggered at some time in response-request cycle.
        Hooks are stored in OrderedDicts.

        :param key: A key that represents type of a hook: `request` or `response` currently supported
        :param name: A name of this particular hook (should be unique among existing hooks of this type)
        :param hook: A callable object that will get .client attribute set to this client
            and that __call__ method takes these params:
                `hook_data` - a request or response objects for different types of hooks
                `**kwargs` - a dict of kwargs that we passed to `request` function of this client
        :return: Nothing
        """

        hooks = self.get_hooks(key)
        hook.client = self
        hooks[name] = hook

    def del_hook(self, key, name):
        """
        Deletes a hook of a specified type and name.

        :param key: A key that represents type of a hook: `request` or `response` currently supported
        :param name: A name of this particular hook
        :return: Nothing
        """

        hooks = self.get_hooks(key)
        hook = hooks.pop(name, None)
        if hook:
            hook.client = None

    @property
    def endpoint(self):
        return self._endpoint

    @endpoint.setter
    def endpoint(self, value):
        self._endpoint = value.rstrip('/') + '/' if value else value

    @property
    def hformat(self):
        return self._hformat

    @hformat.setter
    def hformat(self, value):
        self._hformat = self.prepare_hformat(value)

    @property
    def auth(self):
        return self.session.auth

    @auth.setter
    def auth(self, value):
        self.session.auth = value

    @property
    def verify(self):
        return self.session.verify

    @verify.setter
    def verify(self, value):
        self.session.verify = value

    def prepare_hformat(self, hformat):
        pr_hformat = formats.identify(hformat, raises=True)
        pr_hformat.client = self

        return pr_hformat

    @property
    def auth(self):
        return self.session.auth

    @auth.setter
    def auth(self, value):
        self.session.auth = value

    def request(self, method, uri, **kwargs):
        """
        Performs HTTP request with a specified method and for specified uri

        :param method: An HTTP method name as 'POST', 'GET', etc.
        :param uri: An uri of the resource (relative to the endpoint) or a separate url
        :param kwargs: Any kwargs for `sessions.request` function
        :return: Either a `requests.Response` object or a hypermedia
            object constructed by the hyperclient format
        """

        # Construct full url from the uri
        url = self.construct_url(uri)

        # Adding url and method keys to kwargs
        kwargs['url'], kwargs['method'] = url, method

        log.debug('Making {0} request to [{1}] with args [{2}]'.format(method, url, kwargs))
        # Create a request-response environment that acts like a context
        # Its parts can be changed during hooks processing.
        # It includes kwargs for a request and hformat.
        env = dict(req=kwargs, hformat=self.hformat)

        # Now execute all the `request` hooks on the req object
        self._dispatch_hooks('request', env)

        env['res'] = self.session.request(**kwargs)

        # Execute all the `response` hooks on the res object
        self._dispatch_hooks('response', env)

        return env['res']

    def construct_url(self, uri):
        """This method constructs a full url from a relative uri or a relative path from api endpoint"""
        return urljoin(self.endpoint, uri)
