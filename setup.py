#!/usr/bin/env python

import re
import sys

from setuptools import setup
from setuptools.command.test import test as TestCommand


class PyTest(TestCommand):
    user_options = [('pytest-args=', 'a', "Arguments to pass into py.test")]

    def initialize_options(self):
        TestCommand.initialize_options(self)
        self.pytest_args = []

    def run_tests(self):
        import pytest

        errno = pytest.main(self.pytest_args)
        sys.exit(errno)

packages = [
    'hyperclient',
    'hyperclient.formats',
]

requires = ['requests>=2.9.0']
test_requirements = ['pytest>=2.9.0', 'pytest-httpbin>=0.2.3']

with open('hyperclient/__init__.py', 'r') as fd:
    version = re.search(r'^__version__\s*=\s*[\'"]([^\'"]*)[\'"]',
                        fd.read(), re.MULTILINE).group(1)

if not version:
    raise RuntimeError('Cannot find version information')

setup(
    name='hyperclient',
    version=version,
    description='HTTP client for Hypermedia APIs',
    author='Timur Gergiya',
    author_email='gergia@gmail.com',
    packages=packages,
    package_dir={'hyperclient': 'hyperclient'},
    install_requires=requires,

    cmdclass={'test': PyTest},
    tests_require=test_requirements,
)
